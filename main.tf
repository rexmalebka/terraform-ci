provider "aws" {
  region = "us-west-2"
  access_key = var.access_key 
  secret_key = var.secret_key
}

resource "aws_s3_bucket" "bucket-dorian" {
  bucket = "bucket-tf-dorian"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::bucket-tf-dorian/*"
        }
    ]
}
EOF
  acl = "public-read"

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  tags = {
    Name = "dorian-bucket-tf-test-gatitos"
    Environment = "production"
  }
}

resource "aws_s3_bucket_object" "index" {
  bucket = aws_s3_bucket.bucket-dorian.id
  key    = "index.html"
  source    = "index.html"
  content_type = "text/html"
}

resource "aws_s3_bucket_object" "cat-jpg" {
  bucket = aws_s3_bucket.bucket-dorian.id
  key    = "cat.jpg"
  source    = "cat.jpg"
  content_type = "image/jpeg"
}

resource "aws_s3_bucket_object" "error" {
  bucket = aws_s3_bucket.bucket-dorian.id
  key    = "error.html"
  source    = "error.html"
  content_type = "text/html"
}

resource "aws_s3_bucket_object" "error-jpg" {
  bucket = aws_s3_bucket.bucket-dorian.id
  key    = "error.jpg"
  source    = "error.jpg"
  content_type = "image/jpeg"
}

output "website-endpoint" {
  value = aws_s3_bucket.bucket-dorian.website_endpoint
}

variable "access_key"{
  type = string
}

variable "secret_key"{
  type = string
}
